<?php

function pre($data) {
    $r = print_r($data, true);
    echo '<pre>';
    echo print($r);
    echo '</pre>';
    die();
}

function getInputData() {
    $rawData = file_get_contents("php://input");
    $result =  (array)json_decode($rawData);
    if(empty($result)) return [];
    return $result;
}

function getError($data) {
    getResponse($data, 'error');
}

function getDebug($data) {
    getResponse($data, 'debug');
}

function getResponse($data, $name = 'data') {
    die(json_encode([$name => $data], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

function myCustomErrorHandler(int $errNo, string $errMsg, string $file, int $line) {
    $phpInput = getInputData();
    $error = [
        'err_no'    => $errNo,
        'message'   => $errMsg,
        'file'      => $file,
        'line'      => $line,
        'php-input' => $phpInput,
        '$_GET' => $_GET,
    ];
    getError($error);
}

function routerInit($fName = 'PATH_INFO') {
    $server = $_SERVER;
    $routes = array();
    if(!empty($server[$fName])) {
        $pathInfo = $server[$fName];
        $pathInfo = trim($pathInfo, '/');
        $routes   = explode('/', $pathInfo);
    }

    return $routes;
}

function saveConfig($newData, $oldData, $fileName = 'config/conf.php', $confDir = 'config') {

    $newStringData = renderArrayToString($newData);
    $oldStringData = renderArrayToString($oldData);

    // сохраняем старый конфиг
    $oldFileName = $confDir . '/old_config/conf_' . date("Y_m_d___H_i_s") . '.txt';

    if(empty($oldData))  return false;

    file_put_contents($oldFileName, $oldStringData);

    if(empty($newData))  return false;

    // перезаписываем новый конфиг
    file_put_contents($fileName, $newStringData);

    return true;
}

// --- записывает данные пользователя в файл
function saveFileUserConfig($newData, $fileName = 'config/dbUsers/') {
    $userName = $newData['username'];
    $newData['create_date'] = date("Y_m_d___H_i_s");
    $fileName = $fileName . $userName. '.php';
    $newStringData = renderArrayToString($newData);
    file_put_contents($fileName, $newStringData);
    return true;
}

// --- получаем  данные пользователей из файлов
function getFileUsersConfig($dirName = 'config/dbUsers/') {

    $result = array();
    $files = scandir($dirName);

    foreach($files as $key => $name) {
        if($name == '.' || $name == '..') continue;
        $item = include $dirName . $name;
        $arrName = explode('.', $name);
        $userName = $arrName[0];
        $result[$userName] = $item;
    }

    // print_r($result); die;
    return $result;
}

// --- получаем дефолтные (базовые) настройки
function getDefaultConfig() {
    $config = include CONF_DIR .'/config_default.php';
    return $config;
}

function renderArrayToString($arrData) {
    $ch = 0;
    $stringData = "";
    foreach ($arrData as $key => $value) {
        ($ch) ? $sep = ',' :  $sep = '';
        $stringData .= "{$sep}'{$key}' => '{$value}' \n";
        $ch++;
    }
    $stringData = "<?php \n return array(\n {$stringData}); \n";
    return $stringData;
}

function s2($data) {

    $fileName = "my_file.php";
    $stringData = serialize($data);
    file_put_contents($fileName, $stringData);
    $stringData = file_get_contents($fileName);
    $configData = unserialize($stringData);
    return $configData;
}


function textAreaContentRender($text) {
    $result = explode("\n", $text);
    return $result;
}


function lg() {

    $debugTrace = debug_backtrace();
    $args = func_get_args();

    $get = false;
    $output = $traceStr = '';

    $style = 'margin:10px; padding:10px; border:3px red solid;';

    foreach ($args as $key => $value) {
        $itemArr = array();
        $itemStr = '';
        is_array($value) ? $itemArr = $value : $itemStr = $value;
        if ($itemStr == 'get') $get = true;
        $line = print_r($value, true);
        $output .= '<div style="' . $style . '" ><pre>' . $line . '</pre></div>';
    }

    foreach ($debugTrace as $key => $value) {
        // if($key == 'args') continue;
        $itemArr = array();
        $itemStr = '';
        is_array($value) ? $itemArr = $value : $itemStr = $value;
        if ($itemStr == 'get') $get = true;
        $line = print_r($value, true);
        $output .= '<div style="' . $style . '" ><pre>' . $line . '</pre></div>';
    }


    if ($get)  return $output;

    print $output;
    //print '<pre>' . print_r($debug) . '</pre>';
    die ;

}


function benchmark(&$storage_variable, $name = FALSE, $microseconds = FALSE) {

    if (empty($storage_variable)) { // первый запуск
        $storage_variable = [
            'recent' => [
                'time' => microtime(1),
                'memory' => 0,
                'memory_peak' => 0,
            ],
            'total' => [
                'time' => 0,
                'memory' => 0,
                'memory_peak' => 0,
            ],
        ];
    }

    // Current присваиваем после, а то в time может быть -0 - некрасиво
    $current = [
        'time' => microtime(1),
        'memory' => memory_get_usage(),
        'memory_peak' => memory_get_peak_usage(),
    ];

    $diff = [];

    foreach ($current as $key => $value) {
        $d = $value - $storage_variable['recent'][$key];
        if ($key == 'time')
            $d = round( ( ($microseconds) ? 1000000 : 1000) * $d);
        else
            $d = round($d/1024/1024, 2);

        // (когда функция будет преобразована в класс, округлять при сборе данных не будем вообще,
        // а перенесём это в метод, который будет отвечать за вывод и представление данных)
        $storage_variable['total'][$key] += $d;
        $diff[$key] = $d;

    }

    if ($name) {
        $e = explode(':', $name, 2);
        $keystring = $e[0]; // ключ
        $T = (isset($e[1])) ? $e[1] : ''; // название
        unset($e);

        $tmp = &$storage_variable;

        foreach (explode('.', $keystring) as $K) {

            $tmp = &$tmp['list']; // Нужно для проверки автоинкремент

            if ($K == '%+') { // автоинкрементная нумерация
                $K = ($tmp)
                    ? max(array_keys($tmp)) + 1
                    : 0;
                $T = str_replace('%+', $K, $T); // вложенный + не получится; ну и ладно...
            }

            $tmp = &$tmp[$K];

            if (!$tmp)
                $tmp = $diff;
            else
                foreach ($diff as $key => $value)
                    if ($key == 'title')
                        $tmp[$key] = $value;
                    else
                        $tmp[$key] += $value;
        }

        if ($T) // А title, если есть, присваиваем только
            $tmp['title'] = $T; // последнему уровню вложенности.

        unset($tmp);
    }

    $storage_variable['recent'] = $current;

    return $storage_variable;
}

function customFatalErrorHanler() {

    $error = error_get_last();

    if (is_array($error) &&  in_array($error['type'], [E_ERROR, E_PARSE, E_CORE_ERROR, E_COMPILE_ERROR])) {
        while (ob_get_level()) {
            ob_end_clean();
        }

        $errorTypes = array (
            E_ERROR              => 'Фатальная ошибка',
            E_WARNING            => 'Предупреждение',
            E_PARSE              => 'Ошибка разбора исходного кода',
            E_NOTICE             => 'Уведомление',
            E_CORE_ERROR         => 'Ошибка ядра',
            E_CORE_WARNING       => 'Предупреждение ядра',
            E_COMPILE_ERROR      => 'Ошибка на этапе компиляции',
            E_COMPILE_WARNING    => 'Предупреждение на этапе компиляции',
            E_USER_ERROR         => 'Пользовательская ошибка',
            E_USER_WARNING       => 'Пользовательское предупреждение',
            E_USER_NOTICE        => 'Пользовательское уведомление',
            E_STRICT             => 'Уведомление времени выполнения',
            E_RECOVERABLE_ERROR  => 'Отлавливаемая фатальная ошибка'
        );

        $errNo      = $error['type'];
        $errMessage = $error['message'];
        $errFile    = $error['file'];
        $errLine    = $error['line'];
        $errConst   = $errorTypes[$errNo];
        $requestUri = $_SERVER['REQUEST_URI'];

        $today = date('Y.m.d__H:i:s');
        $errorLog  = "Date : {$today} \n";
        $errorLog .= "Type : PHP ERROR \n";
        $errorLog .= "RequestUri : {$requestUri} \n";
        $errorLog .= "Title: Обработчик ошибок (allErrorHanler) \n";
        $errorLog .= "Place: line={$errLine}; file={$errFile}\n";
        $errorLog .= "ErrNo: {$errNo} \n";
        $errorLog .= "ErrConst: {$errConst} \n";
        $errorLog .= "Message :  {$errMessage}\n";
        $errorLog .= "\n";

        logFileSave($errorLog);
    }
}

function logFileSave($logData, $logFileName = INFO_LOG){
    $logFilePath = LOGS_DIR . '/' . $logFileName;
    $data = $logData;
    if(file_exists($logFilePath)) {
        $current = file_get_contents($logFilePath);
        $data = $logData . $current;
    }
    $save = file_put_contents($logFilePath, $data,  LOCK_EX);
    return $save;
}

//--- Функция обработки ошибок
function customErrorWarningHandler($errNo, $errMessage, $errFile, $errLine) {

    // может потребоваться экранирование $errstr:
    $errMessage = htmlspecialchars($errMessage);
    $requestUri = $_SERVER['REQUEST_URI'];

    $today = date('Y.m.d__H:i:s');
    $errorLog  = "Date : {$today} \n";
    $errorLog .= "Type : PHP ERROR \n";
    $errorLog .= "RequestUri : {$requestUri} \n";
    $errorLog .= "Title: Обработчик ошибок (customErrorWarningHandler) \n";
    $errorLog .= "Place: line={$errLine}; file={$errFile}\n";
    $errorLog .= "ErrNo: {$errNo} \n";
    $errorLog .= "Message :  {$errMessage}\n";
    $errorLog .= "\n";

    logFileSave($errorLog);

}


function curlJsonPost($url, $postData) {

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData, JSON_UNESCAPED_UNICODE));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    $result = curl_exec($ch);
    curl_close($ch);

    $result = json_encode($result, JSON_UNESCAPED_UNICODE);
    return $result;
}

function curlGet($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}

function curlPost($url, $postData){
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData, '', '&'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}