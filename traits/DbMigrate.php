<?php

namespace App\Service\Db\Management\Traits;

trait DbMigrate {

    public function export($params = []) {
        $result = [];
        $dbName = (!empty($params['db_name'])) ? $params['db_name'] : '';
        $migrateTableDir = $this->migrateFolder( $dbName . '/tables');
        $insertTableDir  = $this->migrateFolder( $dbName . '/insert');

        // Таблицы
        $filesIterator = new \DirectoryIterator($migrateTableDir);
        foreach ($filesIterator as $fileInfo) {
            if($fileInfo->isDot()) continue;
            $fileFullPath =  $fileInfo->getPathName();
            $fileName =  $fileInfo->getFilename();
            $this->exportCreateTable($fileFullPath, $fileName);
        }

        // Данные
        $filesIterator = new \DirectoryIterator($insertTableDir);
        foreach ($filesIterator as $fileInfo) {
            if($fileInfo->isDot()) continue;
            $fileFullPath =  $fileInfo->getPathName();
            $fileName     =  $fileInfo->getFilename();
            $this->exportInsertData($fileFullPath, $fileName);
        }

        return $result;
    }

    public function import() {

        // benchmark($BenchMark, 'Старт');
        $migrateTableDir = $this->migrateFolder( 'tables', 'set');
        $insertTableDir  = $this->migrateFolder( 'insert', 'set');
        $tablesInfo = $this->getTablesInfo();

        $ch = 0;
        $result = [];
        // Таблицы
        foreach ($tablesInfo as $tabName => $tabFields) {
            $this->importTableFields($tabFields, $tabName, $migrateTableDir);
            $ch++;
        }

        $ch = 0;
        // Данные
        foreach ($tablesInfo as $tabName => $tabFields) {
            $this->importTableData($tabName, $insertTableDir, $tabFields);
            $ch++;
        }

        return $result;
    }

    protected function exportCreateTable($fileFullPath, $fileName = '') {

        $tableFields = require $fileFullPath;
        $queryItem = [];
        $tableName = (explode('.', $fileName)[0]);

        $dropSql = '';
        $sep = '"';
        if($this->driver == 'mysql') {
            $primaryKeyType = "PRIMARY KEY AUTO_INCREMENT";
            $dropSql  = "DROP TABLE IF EXISTS `{$tableName}`;";
            $sep = "`";
        } else {
            $primaryKeyType = "SERIAL";
        }

        foreach ($tableFields as $fname => $item) {

             $name = $item['name'];
             $type = $item['type'];
             $size = $item['size'];
             $auto = $item['auto'];
             $isNull = $item['is_null'];

             if($type == 'varchar') {
                 $size = (!$size) ? 255 : $size;
                 $type = $type . "({$size})";
             }

            $isNullable = ($isNull == 'YES') ? "NULL" : "NOT NULL";
            $primary = '';

             if($auto) {
                 $primary = $primaryKeyType;
                 $isNullable = '';
                 if($this->driver == 'pgsql') $type = '';
             }

             $sql = "{$sep}{$name}{$sep} {$type} {$primary} {$isNullable} \n";
             $queryItem[] = $sql;
        }


        $query = $dropSql;
        $query .= "CREATE TABLE {$sep}{$tableName}{$sep}  ( \n \t";
        $query .= implode("\t ,", $queryItem);
        $query .= "); \n";

        $save = $this->make($query);
        return $save;
    }

    protected function exportInsertData($fileFullPath, $fileName = '') {

        $tableName = (explode('.', $fileName)[0]);
        $this->make("TRUNCATE TABLE {$tableName}");

        if(is_dir($fileFullPath)) {
            $filesIterator = new \DirectoryIterator($fileFullPath);
            foreach ($filesIterator as $fileInfo) {
                if($fileInfo->isDot()) continue;
                $fileFullPath =  $fileInfo->getPathName();
                $insertDataList = include $fileFullPath;
                foreach ($insertDataList as $item) {
                    $query = $this->fromArrayToSql($item, $tableName);
                    $save = $this->make($query);
                }
            }
        } else {
            $insertDataList = include $fileFullPath;
            foreach ($insertDataList as $item) {
                $query = $this->fromArrayToSql($item, $tableName);
                $save = $this->make($query);
            }
        }
    }

    /**********************
     * @param $item [''name' => 'Bob', 'age' => 32]
     * @param tabName имя таблицы
     * return $sql  INSERT INTO $tabName (`name`, `age`)  VALUES ('Bob', 32)
     */
    protected function fromArrayToSql($item, $tabName) {
         $sep = ($this->driver == 'mysql') ? $sep = "`" : $sep = '"';
         $fields = $values = [];
         foreach ($item as $fname => $value) {
             $value = trim($value);
             if(!$value) $value = 'NULL';
             else {
                 if (!is_numeric($value)) {
                     $value = "'$value'";
                 }
             }

             $values[] = $value;
             $fields[] = "{$sep}$fname{$sep}";
         }

         $valuesSql = implode(", ", $values);
         $fieldSql = implode(", ", $fields);
         $sql = "INSERT INTO {$tabName} \n\t\t\t ({$fieldSql}) \n\t\t VALUES ({$valuesSql})";
         return $sql;
    }

    protected function importTableFields($fields, $tabName, $migrateTableDir) {

        $fieldElem = [];
        foreach ($fields as $fname => $item) {

            $type   = $this->fieldTypeConvert($item['field_type']);
            $auto   = $item['auto_increment'];
            $isNull = $item['is_nullable'];
            $size   = (!empty($item['character_maximum_length'])) ?
                       $item['character_maximum_length'] : '';

            $field = [
                 "\t\t'name'    => '$fname',"
                ,"\t\t'type'    => '$type',"
                ,"\t\t'size'    => '$size',"
                ,"\t\t'auto'    => '$auto',"
                ,"\t\t'is_null' => '$isNull',"
            ];

            $list = implode("\n", $field);
            $fieldElem[$fname] = "\t'$fname' => array(\n$list\n \t), \n";
        }

        $tableInfo = implode("\n", $fieldElem);
        $fileContent = "<?php return [\n $tableInfo ]; \n";
        $filePath = $migrateTableDir . $tabName . '.php';
        $save     = $this->fileSave($filePath, $fileContent);
        return $save;
    }

    protected function insertDataFormatted($data, $tabName, $autoIncName) {
        $insertData = [];
        foreach ($data as  $item) {
            if(isset($item[$autoIncName])) unset($item[$autoIncName]);
            $values = var_export($item, true);
            $insertData[] = "\n " .$values. ",";
        }
        return $insertData;
    }

    protected function importTableData($tabName, $insertTableDir, $fieldsList = []) {

        $counts = $this->selectOne("SELECT COUNT(*) FROM {$tabName} LIMIT 1");

        $allCounts = 0;
        if(!empty($counts['COUNT(*)'])) {
            $allCounts = $counts['COUNT(*)'];
        } elseif(!empty($counts['count'])) {
            $allCounts = $counts['count'];
        }

        if(!$allCounts) return false;

        $autoIncName = $save = '';
        foreach ($fieldsList as $fname => $elem) {
            if(empty($elem['auto_increment'])) continue;
            $autoIncName = $fname;
            break;
        }

        $maxLimit = 60000;
        $itemsCount = $allCounts;
        $orderBy = ($autoIncName) ? "ORDER BY {$autoIncName}" : '';

        if($itemsCount < $maxLimit) {

            $tableData = $this->select("SELECT * FROM {$tabName} {$orderBy}"); // получаем данные из базы
            $insertData = $this->insertDataFormatted($tableData, $tabName, $autoIncName); // формируем данные для миграции
            $varExport = implode("\n", $insertData);
            $fileContent = "<?php return array(\n " . $varExport. " \n); ";

            $filePath = $insertTableDir . $tabName . '.php';
            $save = $this->fileSave($filePath, $fileContent);

        } else {

            $insertTableDir = $insertTableDir . '/' .$tabName;
            if (!file_exists($insertTableDir))
                mkdir($insertTableDir, 0777, true);

            $pageNo = 1;
            $totalPageCount = ceil($itemsCount / $maxLimit);

            for($i = 0; $i < $totalPageCount; $i++) {
                $offset = ($pageNo-1) * $maxLimit;
                $sql = "SELECT * FROM {$tabName} {$orderBy} LIMIT {$maxLimit} OFFSET {$offset}";
                $tableData = $this->select($sql); // получаем данные из базы
                $pageNo++;

                $insertData = $this->insertDataFormatted($tableData, $tabName, $autoIncName); // формируем данные для миграции
                $varExport = implode(', ', $insertData);
                $fileContent = "<?php return array(\n " . $varExport. " \n); ";

                $filePath = $insertTableDir . '/file-' . ($pageNo - 1) . '.php';
                $save = $this->fileSave($filePath, $fileContent);
            }

        }

        return $save;
    }

    protected function migrateFolder($name = 'tables', $type = 'get') {
        $dbName     = $this->dbName;
        $migrateDir = MIGRATE_DIR . '/' .$dbName. '/' .$name. '/';
        switch($type) {
            case 'set' :
                if (!file_exists($migrateDir))
                    mkdir($migrateDir, 0777, true);
                break;

            default :
                $migrateDir = MIGRATE_DIR . '/' . $name;
                if (!file_exists($migrateDir))
                    $migrateDir = MIGRATE_DIR . '/';
                break;

        }
        return $migrateDir;
    }

    protected function fieldTypeConvert($columnType) {

        $fieldTypes = [
            'integer' => 'int',
            'character varying' => 'varchar',
            'timestamp without time zone' => 'timestamp',
            'smallint' => 'int',
            'tinytext' => 'text',
            'tinyint'  => 'int',
            'char'     => 'varchar',
            'bigint'   => 'bigint',
            'longtext' => 'text',
            'enum'     => 'enum',
            'double'   => 'double',
        ];

        $result = $columnType;
        if(isset($fieldTypes[$columnType]))
            $result = $fieldTypes[$columnType];
        return $result;
    }

}