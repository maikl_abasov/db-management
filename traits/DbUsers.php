<?php

namespace App\Service\Db\Management\Traits;

use App\Service\Db\Management\CustomErrorHandler;

trait DbUsers {

    // получаем всех пользователей
    protected function getDbUsersList(){
        $variants = [
            'mysql' => "SELECT * FROM mysql.user",
            'pgsql' => "SELECT * FROM pg_user",
        ];
        $query = $this->isDriver($variants);
        return $this->select($query);
    }

    // получаем текущего пользователя
    protected function getCurrentDbUser(){
        $variants = [
            'mysql' => "SELECT db, host, user FROM mysql.db",
            'pgsql' => "SELECT * FROM pg_stat_activity",
        ];
        $query = $this->isDriver($variants);
        return $this->select($query);
    }

    // Создать нового пользователя
    protected function createUser($params) {
        $userName = $this->isParam($params, 'username');
        $password = $this->isParam($params, 'password');
        $superUserStatus = $this->isParam($params, 'set_super_user');
        if(!$password)  $password = '123456';

        $variants = [
             'mysql' => "CREATE USER '{$userName}'@'%' IDENTIFIED BY '{$password}';",
             'pgsql' => "CREATE USER {$userName} WITH PASSWORD '{$password}';",
        ];

        $query = $this->isDriver($variants);
        $result = $this ->make($query);

        if($superUserStatus) {
            $set = $this->setSuperUserStatus($params);
        }

        return $result;
    }

    // Установить суперправа пользователю
    protected function setSuperUserStatus($params) {
        $userName = $this->isParam($params, 'username');
        $curDbName = $this->dbName;
        $variants = [
            'mysql' => "GRANT ALL PRIVILEGES ON {$curDbName}.* TO '{$userName}'@'%';",
            'pgsql' => "ALTER USER {$userName} WITH SUPERUSER;",
        ];
        $query = $this->isDriver($variants);
        $result = $this->make($query);
        // $result = $this->make('FLUSH PRIVILEGES');
        return $result;
    }

    // Посмотреть права пользователя
    protected function getUserPrivilegies($params) {
        $userName = $this->isParam($params, 'username');
        $variants = [
            'mysql' => "SHOW GRANTS FOR '{$userName}'@'%';",
            'pgsql' => "SELECT * FROM information_schema.table_privileges WHERE grantee = '{$userName}';",
        ];
        $query = $this->isDriver($variants);
        $result = $this->select($query);
        return $result;
    }


    // Удаляем пользователя
    protected function deleteDbUser($params){
        $userName = $this->isParam($params, 'username');
        switch($userName){
            case  'postgres' :
                  $warnMessage = "Пользователя {$userName} нельзя удалить , это системный пользователь";
                  throw new CustomErrorHandler($warnMessage, 5531, __FILE__, __LINE__);
                  return false;
        }

        $query = "DROP USER {$userName}";
        $response = $this->make($query);
        return $response;
    }



    /////////////////////////////
    /// СТАРЫЙ КОД
    ///
    ///
    ///


    // Установить привилегии пользователю
    protected function setUserPrivileges($userName, $dbName = '') {
        if((is_array($userName)) || (empty($userName))) {
            $userName = $this->isParam(0);
            $dbName   = $this->isParam(1);
        }
        $query = "GRANT ALL PRIVILEGES ON DATABASE {$dbName} TO {$userName};";
        $result = $this -> exec($query);
        return array('save_result' => $result);
    }

    // Удалить привилегии пользователю к базе данных
    protected function delUserPrivileges($userName, $dbName = '') {
        if((is_array($userName)) || (empty($userName))) {
            $userName = $this->isParam(0);
            $dbName   = $this->isParam(1);
        }
        $query = "REVOKE ALL PRIVILEGES ON DATABASE {$dbName} FROM {$userName};";
        $result = $this -> exec($query);
        return array('save_result' => $result);
    }

    // Удалить суперправа пользователю
    protected function delSuperUser($userName = '') {
        if((is_array($userName)) || (empty($userName))) {
            $userName = $this->isParam(0);
        }

        switch($userName){
            case  'w1user' :
            case  'postgres' :
                die("С пользователя {$userName} нельзя снимать привилегии");
                return;
        }

        $query = "ALTER USER {$userName} WITH NOSUPERUSER;";
        $result = $this -> exec($query);
        return $result;
    }

    protected function changeUserPassword($userName, $newPassword) {

        $query = "ALTER USER {$userName} WITH PASSWORD '{$newPassword}'";
        $result = $this->exec($query);

        $users = getFileUsersConfig();
        foreach ($users as $name => $user) {
            if($name != $userName)  continue;
            $user['passwd'] = $newPassword;
            $user['update_date'] = date("Y-m-d H:i:s");
            saveFileUserConfig($user);
            break;
        }

        return $result;
    }

    protected function clearFileUsersConfigDir($dirName = 'config/dbUsers/') {
         $dbUsers = $this->getDbUsersList();
         $fileUsers = getFileUsersConfig();

         foreach ($dbUsers as $key => $user) {
             $name = $user['usename'];
             if(!empty($fileUsers[$name]))
                 unset($fileUsers[$name]);
         }

        foreach ($fileUsers as $name => $user) {
            if(($name == 'admin') || ($name == 'w1user'))
                continue;

            $userFile = $dirName . $name . '.php';
            if(file_exists($userFile))
                unlink($userFile);
        }
    }

}
