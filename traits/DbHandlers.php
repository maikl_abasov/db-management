<?php

namespace App\Service\Db\Management\Traits;

use App\Service\Db\Management\CustomErrorHandler;

trait DbHandlers {

    // -- Получаем все базы на сервере
    protected function getDatabaseList(){
        $variants = [
            'mysql' => "SHOW DATABASES;",
            'pgsql' => "SELECT * FROM pg_database;",
        ];
        $query = $this->isDriver($variants);
        return $this->select($query);
    }

    // -- Получаем текущею базу
    protected function getСurrentDatabase(){
        $variants = [
            'mysql' => "SELECT DATABASE();",
            'pgsql' => "SELECT current_database();",
        ];
        $query = $this->isDriver($variants);
        return $this->select($query);
    }

    // -- Создаем новую базу
    protected function createDatabase($params){
        $dbName = $this->isParam($params, 'db_name');
        if(!$dbName) {
            throw new CustomErrorHandler('Создание базы: не задано имя базы', 5531, __FILE__, __LINE__, []);
        }
        $query = "CREATE DATABASE {$dbName}";
        $result = $this->make($query);
        return $result;
    }

    // -- Удаляем базу
    protected function dropDb($params){
        $dbName = $this->isParam($params, 'db_name');
        $query = "DROP DATABASE {$dbName}";
        $result = $this->make($query);
        return $result;
    }

    // -- Создаем базу и прикрепляем пользователя
    protected function createDbNextBindUser($params){
        $dbName = $this->isParam($params, 'db_name');
        $ownerName = $this->isParam($params, 'username');
        if($this->driver == 'pgsql') {
            $query = "CREATE DATABASE {$dbName} OWNER {$ownerName}";
            return $this->make($query);
        }
        return false;
    }

    // -- Копируем базу
    protected function copyDb($params){
        $dbName    = $this->isParam($params, 'db_name');
        $ownerName = $this->isParam($params, 'username');
        $newDbName = $this->isParam($params, 'new_db_name');
        if($this->driver == 'pgsql') {
            $query = "CREATE DATABASE {$newDbName} WITH TEMPLATE {$dbName} OWNER {$ownerName};";
            return $this->make($query);
        }
        return false;
    }

    // -- Создаем базу и создаем пользователя
    protected function createDbNextCreateUser($params){
        $dbName    = $this->isParam($params, 'db_name');
        $userName  = $this->isParam($params, 'username');
        $password  = $this->isParam($params, 'password');

        if(!$userName) $userName = $dbName;
        if(!$password) $password = '123456';

        if($this->driver == 'pgsql') {
            $query = "CREATE DATABASE {$dbName};";
            $this->make($query);
            $query = "CREATE USER {$userName} WITH ENCRYPTED PASSWORD '{$password}';";
            $this->make($query);
            $query = "GRANT ALL PRIVILEGES ON DATABASE {$dbName} TO {$userName};";
            return $this->make($query);
        }
        return false;
    }

}
