<?php

namespace App\Service\Db\Management\Traits;

trait DbTableHandlers {

    // -- Создаем новую таблицу
    protected function createTable($params = []) {

        $tableName = $this->isValue($params, 'table_name');
        $idName    = $this->isValue($params, 'id_name', 'id');

        $primaryKeyType = $this->isDriver([
            'mysql' => "int PRIMARY KEY AUTO_INCREMENT",
            'pgsql' => "SERIAL PRIMARY KEY",
        ]);

        if(!$tableName) {
            $error = json_encode(['message' => 'Создание таблицы:нет имени таблицы']);
            throw new \ErrorException($error, 1005);
        }

        $query = "CREATE TABLE {$tableName} ({$idName} {$primaryKeyType})";
        return $this ->make($query);
    }

    // -- Изменяем имя таблицы
    protected function renameTable($params){

        $tableName = $this->isValue($params, 'table_name');
        $newTableName = $this->isValue($params, 'new_table_name');

        if(!$tableName) {
            $error = json_encode(['message' => 'Изменение имени таблицы:нет имени таблицы']);
            throw new \ErrorException($error, 1006);
        }

        if(!$newTableName) {
            $error = json_encode(['message' => 'Изменение имени таблицы:нет нового имени таблицы']);
            throw new \ErrorException($error, 1007);
        }

        $query = "ALTER TABLE {$tableName} RENAME TO {$newTableName};";
        return $this ->make($query);
    }

    // -- Удаляем таблицу
    protected function dropTable($params){
        $tableName = $this->isValue($params, 'table_name');
        if(!$tableName) {
            $error = json_encode(['message' => 'Удаление таблицы:нет имени таблицы']);
            throw new \ErrorException($error, 1008);
        }
        $query = "DROP TABLE {$tableName};";
        return $this ->make($query);
    }

    // -- Добавляем новое поле
    protected function addField($params) {

        $tableName = $this->isValue($params, 'table_name');
        $fname = $this->isValue($params, 'field_name');
        $type  = $this->isValue($params, 'type', 'varchar');
        $size  = $this->isValue($params, 'size');
        $isNull  = $this->isValue($params, 'is_null');

        $isNull = ($isNull == 1) ? $isNull = 'NULL' : '';

        if($type == 'varchar' || $type == 'VARCHAR') {
            $size = ($size) ? '(' . $size .')' : '(255)';
        }

        if(!$fname) {
            $error = json_encode(['message' => 'Новое поле: нет имени поля']);
            throw new \ErrorException($error, 1009);
        }

        if(!$tableName) {
            $error = json_encode(['message' => 'Новое поле: нет имени таблицы']);
            throw new \ErrorException($error, 10010);
        }

        $query = "ALTER TABLE {$tableName} ADD COLUMN  {$fname} {$type}{$size} {$isNull};";

        return $this->make($query);
    }


    // -- Изменяем имя поля
    protected function renameField($params) {

        $tableName    = $this->isValue($params, 'table_name');
        $oldFieldName = $this->isValue($params, 'old_name');
        $newFieldName = $this->isValue($params, 'new_name');

        if(!$tableName) {
            $error = json_encode(['message' => 'Изменение поля:нет имени таблицы']);
            throw new \ErrorException($error, 1006);
        }

        if(!$oldFieldName) {
            $error = json_encode(['message' => 'Изменение поля: нет старого имени поля']);
            throw new \ErrorException($error, 1011);
        }

        if(!$newFieldName) {
            $error = json_encode(['message' => 'Изменение поля: нет нового имени поля']);
            throw new \ErrorException($error, 10012);
        }

        $query = "ALTER TABLE {$tableName} RENAME COLUMN {$oldFieldName} TO {$newFieldName};";
        return $this ->make($query);
    }


    // -- Удаляем поле
    protected function dropField($params) {

        $tableName = $this->isValue($params, 'table_name');
        $fieldName = $this->isValue($params, 'field_name');

        if(!$tableName) {
            $error = json_encode(['message' => 'Удаление поля:нет имени таблицы']);
            throw new \ErrorException($error, 1012);
        }

        if(!$fieldName) {
            $error = json_encode(['message' => 'Удаление поля: нет имени поля']);
            throw new \ErrorException($error, 1013);
        }

        $query = "ALTER TABLE {$tableName} DROP  COLUMN {$fieldName};";
        $result = $this ->make($query);
        return $result;
    }

    // -- Изменяем тип поля
    protected function changeFieldType($params) {

        $tableName = $this->isValue($params, 'table_name');
        $fieldName = $this->isValue($params, 'field_name');
        $newType   = $this->isValue($params, 'new_type');

        if(!$tableName) {
            $error = json_encode(['message' => 'Изменение типа поля:нет имени таблицы']);
            throw new \ErrorException($error, 1014);
        }

        if(!$fieldName) {
            $error = json_encode(['message' => 'Изменение типа поля: нет имени поля']);
            throw new \ErrorException($error, 1015);
        }

        if(!$newType) {
            $error = json_encode(['message' => 'Изменение типа поля: нет типа поля']);
            throw new \ErrorException($error, 1016);
        }

        if($this->driver == 'mysql' && ($newType == 'varchar' | $newType == 'VARCHAR')) {
            $newType = $newType . '(255)';
        }

        $query = $this->isDriver([
            'mysql' => "ALTER TABLE {$tableName} MODIFY {$fieldName} {$newType}",
            'pgsql' => "ALTER TABLE {$tableName} ALTER COLUMN {$fieldName} TYPE {$newType} USING ({$fieldName}::{$newType});",
        ]);

        $result = $this ->make($query);
        return $result;
    }


    // -- Добавить индекс
    protected function createIndex($params) {

        $tableName = $this->isValue($params, 'table_name');
        $fieldName = $this->isValue($params, 'field_name');

        if(!$tableName) {
            $error = json_encode(['message' => 'Добавить индекс:нет имени таблицы']);
            throw new \ErrorException($error, 1017);
        }

        if(!$fieldName) {
            $error = json_encode(['message' => 'Добавить индекс: нет имени поля']);
            throw new \ErrorException($error, 1018);
        }

        $query = $this->isDriver([
            'mysql' => "CREATE INDEX {$fieldName} ON {$tableName}",
            'pgsql' => "CREATE INDEX {$fieldName}_idx ON {$tableName} ($fieldName);",
        ]);

        $result = $this ->make($query);
        return $result;
    }

    // -- Удалить индекс
    protected function dropIndex($params) {

        $tableName = $this->isValue($params, 'table_name');
        $fieldName = $this->isValue($params, 'field_name');

        if(!$tableName) {
            $error = json_encode(['message' => 'Удалить индекс:нет имени таблицы']);
            throw new \ErrorException($error, 1019);
        }

        if(!$fieldName) {
            $error = json_encode(['message' => 'Удалить индекс: нет имени поля']);
            throw new \ErrorException($error, 1020);
        }

        $query = $this->isDriver([
            'mysql' => "DROP INDEX {$fieldName}_idx;",
            'pgsql' => "DROP INDEX {$fieldName}_idx;",
        ]);

        $result = $this ->make($query);
        return $result;
    }


    // -- Копирование таблицы
    protected function copyTable($params) {

        $tableName = $this->isValue($params, 'table_name');
        $newTableName = $this->isValue($params, 'copy_table_name');

        if(!$tableName) {
            $error = json_encode(['message' => 'Копирование таблицы:нет имени таблицы']);
            throw new \ErrorException($error, 1022);
        }

        if(!$newTableName) {
            $error = json_encode(['message' => 'Копирование таблицы: нет имени новой таблицы']);
            throw new \ErrorException($error, 1023);
        }

        $query = $this->isDriver([
            'mysql' => "CREATE TABLE {$newTableName} SELECT * FROM {$tableName};",
            'pgsql' => "CREATE TABLE {$newTableName} AS (SELECT * FROM {$tableName});",
        ]);

        $result = $this ->make($query);

        return $result;
    }

}
