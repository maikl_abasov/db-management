
const EditFormWave = {
    props: [
      'title',
      'btn_title',
      'form_item',
      'func_name',
      'name',
      'cols',
      'gap',
    ],
    data() {
        let elemSize = Object.keys(this.form_item).length;
        let formItem = this.form_item;
        let gapSize  = (!this.gap) ?  2 : this.gap;
        let colSize  = (!this.cols) ? 1 : this.cols;
        return {

            gapSize,
            elemSize,
            colSize,
            formItem,

            inputClass: 'block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer',
            labelClass: 'peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6',
            btnClass: 'text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800',

        }
    },

    methods: {
        sendRequestMake() {
            const postData = this.formItem;
            const url = '?fn=' + this.getFuncName;
            this.http.post(url, postData,(response) => {
                console.log(response);
                if(response.queryString) {
                    alert(response.queryString);
                }
                this.$emit('send_response', { response });
            });
        },
    },

    computed: {

        getFormItem() {
            return this.formItem;
        },

        getFuncName() {
            return this.func_name;
        },

        getBtnTitle() {
            return this.btn_title;
        },

        getTitle() {
            return this.title;
        },
    },

    template: `
    <div class="edit-form-item-wave-box" style="border: 1px gainsboro dashed; padding: 4px; width: 100%; " >
    
          <div v-if="getTitle" style="margin: 1px 0px 10px 0px !important; font-style: italic; color:green; 
                                      font-size: 10px; border-bottom: 1px gainsboro solid; ">{{getTitle}}</div>
          
          <div :class="'grid md:grid-cols-'+colSize+' md:gap-' + gapSize" >
          
             <template v-for="(value, fname) in formItem" >
                <div class="relative z-0 mb-6 w-full group" style="margin-bottom: 0px">
                    <input v-model="formItem[fname]" :id="'wave-input-' + fname" type="text"
                           :class="inputClassWave" placeholder=" " required="">
                    <label :for="'wave-input-' + fname" :class="labelClassWave" >{{fname}}</label>
                </div>
             </template>    
              
             <button @click="sendRequestMake()" :class="btnClassWave" type="submit" 
                     style="border-radius: 3px; padding: 0px 8px 0px 15px !important; margin: 0px 0px 4px 0px;
                            font-size: 11px; height: 40px; text-align: center !important; width: 100%;" >{{getBtnTitle}}</button>

          </div>   
    </div>
    `
}