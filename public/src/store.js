
const GlobalStore =  new Vuex.Store({
    state: {
        tableList: [],
    },

    mutations: {
        setTables(state, data) {
            state.tableList = data;
        }
    },

    actions: {
        fetchTables (context) {
            const url = '?fn=getTables';
            httpClient.get(url, (response) => {
                context.commit('setTables', response);
            });
        }
    },

    getters: {
        getTablesList: (state) => state.tableList
    },


});

