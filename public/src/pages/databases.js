
const DataBasePage = {

    data() {
        return {
           dbName: '',
           dbItem: {},
           databaseList: [],
        }
    },

    created() {
        this.getDatabaseList();
    },

    computed: {
        getDbName() {
            return this.dbName;
        },

        getDbItem() {
            return this.dbItem;
        },

        getDbList() {
            return this.databaseList;
        },
    },

    methods: {
        getItem(resp) {
            this.dbName = resp.name;
            this.dbItem = resp.item;
            // this.getFields();
        }
    },

    template: `
 
        <!-- ЛЕВАЯ ПАНЕЛЬ -->
        <div class="hidden lg:block">
        
             <list-render 
                title="Базы"
                name="datname"
                :list="databaseList" 
                @get_item="getItem"
             />

        </div>

        <!-- ПРАВАЯ ПАНЕЛЬ -->
        <div class="lg:col-span-3"> 
            <div class="border-1 border-solid border-gray-200  h-96 lg:h-full"
               style="border: 1px gainsboro solid; padding:10px">

                <!-- Table -->
                <div v-if="getDbName" class="w-full mx-auto bg-white shadow-lg rounded-sm border border-gray-200">
                    <header class="px-5 py-4 border-b border-gray-100">
                        <h2 class="font-semibold text-gray-800">{{getDbName}}</h2>
                    </header>
                    <div class="p-3">
                        <div class="overflow-x-auto">
                            <table class="table-auto w-full">
                            
                                <thead class="text-xs font-semibold uppercase text-gray-400 bg-gray-50">
                                    <tr>
                                        <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">datname</div> </th>
<!--                                        <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">datacl</div> </th>-->
                                        <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">encoding</div> </th>    
                                        <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">datcollate</div> </th>
                                        
                                        <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">datctype</div> </th>
                                        <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">datistemplate</div> </th>
                                        <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">datallowconn</div> </th>
                                        <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">Удалить</div> </th>
                                    </tr>
                                </thead>
                                
                                <tbody class="text-sm divide-y divide-gray-100">
                                
                                    <tr>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="flex items-center">
                                                <div class="font-medium text-gray-800 text-left">{{getDbItem.datname}}</div>
                                            </div>
                                        </td>
<!--                                        <td class="p-2 whitespace-nowrap">-->
<!--                                            <div class="text-left">{{getDbItem.datacl}}</div>-->
<!--                                        </td>-->
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="text-left font-medium text-green-500">{{getDbItem.encoding}}</div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="text-sm text-center">{{getDbItem.datcollate}}</div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="text-md text-center">{{getDbItem.datctype}}</div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="text-md text-center">{{getDbItem.datistemplate}}</div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="text-md text-center">{{getDbItem.datallowconn}}</div>
                                        </td>
                                        <td>
                                           <div @click="dropDb()" class="pointer-events-auto ml-auto font-medium text-indigo-600 hover:text-indigo-500" 
                                                style="cursor:pointer"> Удалить
                                           </div>
                                        </td>
                                    </tr>
                                    
                                    <tr v-if="getDbItem.datacl"><td colspan="7">
                                         datacl: {{getDbItem.datacl}}
                                    </td></tr>
                                    
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
                <!-- ./ Table --> 
   
<!--                 <pre>{{getDbName}}</pre> -->
<!--                 <pre>{{getDbItem}}</pre>-->
<!--                 <pre>{{getDbList}}</pre> -->
            </div>
        </div> 
    `
}
