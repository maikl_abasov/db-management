const TablePage = {
    data() {
        return {
            tableName: '',
            fieldName: '',
            tableItem: [],
            fieldsList: [],
            activeIndex: '',

            columnList: {
               field_name: 'Имя поля',
               field_type: 'Тип',
               character_maximum_length: 'Size',
               is_nullable: 'is_nullable',
               auto_increment: 'auto_increment',
            },
        }
    },

    created() {
        this.getTables();


    },

    mounted() {

        setTimeout(() => {
            for(let i in this.tableList) {
                let item = this.tableList[i];
                this.activeIndex = i;
                this.tableName = item.table_name;
                this.tableItem = item;
                this.getFields();
                break;
            }
        }, 1000);

    },

    computed: {
        getTableName() {
            return this.tableName;
        },

        getTableItem() {
            return this.tableItem;
        },

        getFieldsList() {
            return this.fieldsList;
        },
    },

    methods: {
        getItem(resp) {
            this.tableName = resp.name;
            this.tableItem = resp.item;
            this.getFields();
        },

        sendResponse(response) {
            console.log(response);
        },
    },

    template: `

        <!-- ЛЕВАЯ ПАНЕЛЬ -->
        <div class="hidden lg:block">
        
             <list-render 
                title="Таблицы"
                name="table_name"
                :list="tableList" 
                :active="activeIndex"
                @get_item="getItem" />

        </div>

        <!-- ПРАВАЯ ПАНЕЛЬ -->
        <div class="lg:col-span-3">   
            <div class="border-1 border-solid border-gray-200  h-96 lg:h-full"
                 style="border: 1px gainsboro solid; padding:5px" >
    
                  <div v-if="tableName" 
                       style="display: flex; margin: 0px 0px 10px 17px; border-bottom: 2px gainsboro dotted; padding: 3px 3px 3px 0px">
                  
                         <div v-if="tableName" style="display: flex; width: 40%; border: 0px gainsboro solid; padding: 0px;">
                            <div class="relative" style=" width: 150px">
                                <input v-model="copyTableName" type="text" id="copy_table_name" :class="inputClassWave" placeholder=" " />
                                <label for="copy_table_name" :class="labelClassWave">Имя новой таблицы</label>
                            </div>
                            <a href="#" @click="copyTable()" :class="btnClassWave"
                               style="border-radius: 3px; padding: 0px 8px 0px 8px; font-size: 11px; height: 40px">Копировать таблицу</a>
                         </div>
                         
                         <div style="border-left: 2px gainsboro dotted; margin-left: 10px"></div>   
                         
                         <div v-if="tableName" style="margin-left: auto">
                                  <button @click="dropTable()" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800" 
                                          type="submit" style="height: 42px; margin-left: 10px; border-radius: 4px; font-size: 11px">Удалить таблицу ({{tableName}})</button>
                         </div> 

                  </div>
    
                  <div v-if="tableName" style="display: flex; width: 98%; border-bottom: 2px gainsboro dotted; padding: 3px 3px 3px 0px; margin: 0px 0px 4px 17px">
                  
                        <div class="relative" style=" width: 150px">
                            <input v-model="newField.field_name" type="text" id="create_field_name" :class="inputClassWave" placeholder=" " />
                            <label for="create_field_name" :class="labelClassWave">Имя поля</label>
                        </div>

                        <div class="relative" style="margin-left: 5px; width: 100px">
                            <input v-model="newField.type" type="text" id="create_field_type" :class="inputClassWave" placeholder=" " />
                            <label for="create_field_type" :class="labelClassWave">Тип</label>
                        </div>

                        <div class="relative" style="margin-left: 5px; width: 70px">
                            <input v-model="newField.size" type="text" id="create_field_size" :class="inputClassWave" placeholder=" " />
                            <label for="create_field_size" :class="labelClassWave">Размер</label>
                        </div>
                        
                        <div class="relative" style="margin-left: 5px; width: 70px">
                            <input v-model="newField.is_null" type="text" id="create_field_is_null" :class="inputClassWave" placeholder=" " />
                            <label for="create_field_is_null" :class="labelClassWave"> Is null</label>
                        </div>

                        <a href="#" @click="addField()" :class="btnClassWave"
                           style="border-radius: 3px; padding: 0px 8px 0px 8px; font-size: 11px; height: 40px">Добавить поле</a>
    
                  </div>
    
                  <!-- Table -->
                  <div class="w-full mx-auto bg-white shadow-lg rounded-sm border border-gray-200">
                        <header class="px-5 py-4 border-b border-gray-100">
                            <h2 class="font-semibold text-gray-800">{{tableName}}</h2>
                        </header>
                        <div class="p-3">
                            <div class="overflow-x-auto">
                                <table class="table-auto w-full">
                                
                                    <thead class="text-xs font-semibold uppercase text-gray-400 bg-gray-50">
                                        <tr>
                                            <template v-for="(title, name) in columnList" >
                                              <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">{{title}}</div> </th>
                                            </template>
                                            <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">Удалить</div> </th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody class="text-sm divide-y divide-gray-100">
                                        <template v-for="(item, index) in getFieldsList">  
                                            <tr>
                                                <td class="p-2 whitespace-nowrap">
                                                    <div class="flex items-center">
                                                        <div class="w-10 h-10 flex-shrink-0 mr-2 sm:mr-3"></div>
                                                        <div class="font-medium text-gray-800">{{item.field_name}}</div>
                                                    </div>
                                                </td>
                                                <td class="p-2 whitespace-nowrap">
                                                    <div class="text-left">{{item.field_type}}</div>
                                                </td>
                                                <td class="p-2 whitespace-nowrap">
                                                    <div class="text-left font-medium text-green-500">{{item.character_maximum_length}}</div>
                                                </td>
                                                <td class="p-2 whitespace-nowrap">
                                                    <div class="text-sm text-center">{{item.is_nullable}}</div>
                                                </td>
                                                <td class="p-2 whitespace-nowrap">
                                                    <div class="text-md text-center">{{item.auto_increment}}</div>
                                                </td>
                                                <td>
                                                   <div @click="dropField(item)" class="pointer-events-auto ml-auto font-medium text-indigo-600 hover:text-indigo-500" 
                                                        style="cursor:pointer"> Удалить
                                                   </div>
                                                </td>
                                            </tr>
                                        </template>
                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>
                  </div>
                  <!-- ./ Table -->

                  <!----- ФОРМА -----
                  <div class="relative p-4" style="border: 0px red solid !important;">
                      <div class="-mr-[4.625rem] w-[50.25rem] bg-white p-4 text-[0.8125rem] leading-6 text-slate-900 shadow-xl shadow-black/5 ring-1 ring-slate-700/10" >
                          <div class="font-semibold leading-5" >{{tableName}}</div>
                          <div class="mt-4 flex items-center border-t border-slate-400/20 py-3">
                              <template v-for="(title, name) in columnList" >
                                 <div v-if="name == 'field_name'" class="w-2/6 flex-none">{{title}}</div>
                                 <div v-else class="tab-field-box">{{title}}</div>
                                 <div class="mx-3 h-6 w-px bg-slate-400/20"></div>
                              </template>
                          </div>
                          <template v-for="(item, index) in getFieldsList">     
                              <div class="mt-4 flex border-t border-slate-400/20 py-3" >

                                 <div class="w-2/6 flex-none">{{item.field_name}}</div>
                                 <div class="mx-3 h-6 w-px bg-slate-400/20"></div>
                                 <div class="tab-field-box">{{item.field_type}}</div>
                                 <div class="mx-3 h-6 w-px bg-slate-400/20"></div>
                                 <div class="tab-field-box">{{item.character_maximum_length}}</div>
                                 <div class="mx-3 h-6 w-px bg-slate-400/20"></div>
                                 <div class="tab-field-box">{{item.is_nullable}}</div>
                                 <div class="mx-3 h-6 w-px bg-slate-400/20"></div>
                                 <div class="tab-field-box">{{item.auto_increment}}</div>
                                 
                                 <div @click="dropField(item)" class="pointer-events-auto ml-auto font-medium text-indigo-600 hover:text-indigo-500" style="cursor:pointer">
                                    Удалить
                                 </div>
                              </div>
                          </template>
                  </div></div>
                  ------------>

            </div>
        </div> 
    `
}
