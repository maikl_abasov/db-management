
const UserPage = {

    data() {
        return {
            userName: '',
            userItem: {},
            usersList: [],
        }
    },

    created() {
        this.getDbUsersList();
    },

    computed: {
        getUserName() {
            return this.userName;
        },

        getUserItem() {
            return this.userItem;
        },

        getUsersList() {
            return this.usersList;
        },
    },

    methods: {
        getItem(resp) {
            this.userName = resp.name;
            this.userItem = resp.item;
        }
    },

    template: `
 
        <!-- ЛЕВАЯ ПАНЕЛЬ -->
        <div class="hidden lg:block">
        
             <list-render 
                title="Пользователи"
                name="usename"
                :list="usersList" 
                @get_item="getItem"
             />

        </div>

        <!-- ПРАВАЯ ПАНЕЛЬ -->
        <div class="lg:col-span-3"> 
            <div class="border-1 border-solid border-gray-200  h-96 lg:h-full"
               style="border: 1px gainsboro solid; padding:10px">
    
                <!-- Table -->
                <div v-if="getUserName" class="w-full mx-auto bg-white shadow-lg rounded-sm border border-gray-200">
                    <header class="px-5 py-4 border-b border-gray-100">
                        <h2 class="font-semibold text-gray-800">{{getUserName}}</h2>
                    </header>
                    <div class="p-3">
                        <div class="overflow-x-auto">
                            <table class="table-auto w-full">
                            
                                <thead class="text-xs font-semibold uppercase text-gray-400 bg-gray-50">
                                    <tr>
                                        <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">usename</div> </th>
                                        <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">usesysid</div> </th>
                                        <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">usecreatedb</div> </th>
                                        <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">usesuper</div> </th>
                                        <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">userepl</div> </th>
                                        <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">usebypassrls</div> </th>
                                        <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">passwd</div> </th>
                                        <th class="p-2 whitespace-nowrap"> <div class="font-semibold text-left">Удалить</div> </th>
                                    </tr>
                                </thead>
                                
                                <tbody class="text-sm divide-y divide-gray-100">
                                    <tr>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="flex items-center">
<!--                                                <div class="w-10 h-10 flex-shrink-0 mr-2 sm:mr-3"></div>-->
                                                <div class="font-medium text-gray-800 text-left">{{getUserItem.usename}}</div>
                                            </div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="text-left">{{getUserItem.usesysid}}</div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="text-left font-medium text-green-500">{{getUserItem.usecreatedb}}</div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="text-sm text-center">{{getUserItem.usesuper}}</div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="text-md text-center">{{getUserItem.userepl}}</div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="text-md text-center">{{getUserItem.usebypassrls}}</div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="text-md text-center">{{getUserItem.passwd}}</div>
                                        </td>
                                        <td>
                                           <div @click="deleteDbUser()" class="pointer-events-auto ml-auto font-medium text-indigo-600 hover:text-indigo-500" 
                                                style="cursor:pointer"> Удалить
                                           </div>
                                        </td>
                                    </tr>
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
                <!-- ./ Table -->
               
<!--                 <pre>{{getUserName}}</pre> -->
<!--                 <pre>{{getUserItem}}</pre>-->
<!--                 <pre>{{getUsersList}}</pre> -->
                 
            </div>
        </div> 
    `
}
