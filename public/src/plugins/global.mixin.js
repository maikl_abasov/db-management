
const GlobalMixin = {
    data() {
        return {
            http: httpClient,

            tableList: [],
            databaseList: [],
            usersList:[],
            tablesListInfo: [],
            fieldsList: [],
            userPrivilegies: [],

            tableName : '',
            username  : '',
            fieldName : '',

            inputClassWave: 'block  px-0 pb-0 pt-4 w-full text-sm text-gray-900 bg-gray-50 dark:bg-gray-700 border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer',
            labelClassWave: 'absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-3 z-10 origin-[0] left-2.5 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4',
            btnClassWave: 'hidden md:inline-flex items-center text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium  text-sm px-2 py-0 text-center ml-3',

        }
    },

    created() {

    },

    methods: {

        getTables() {
            const postData = { scheme: 'public' };
            const url = '?fn=getTables';
            this.http.post(url, postData, (response) => {
                this.tableList = response;
            });
        },

        getTablesInfo() {
            const postData = { scheme: 'public' };
            const url = '?fn=getTablesInfo';
            this.http.get(url, postData, (response) => {
                this.tablesListInfo = response;
            });
        },

        getDatabaseList() {
            const url = '?fn=getDatabaseList';
            this.http.get(url, (response) => {
                this.databaseList = response;
            });
        },

        getDbUsersList() {
            const url = '?fn=getDbUsersList';
            this.http.get(url, (response) => {
                this.usersList = response;
            });
        },

        getFields() {
            const postData = { table_name: this.tableName };
            const url = '?fn=getFields';
            this.http.post(url, postData,(response) => {
                this.fieldsList = response;
            });
        },

        getUserPrivilegies() {
            const postData = { username: this.username };
            const url = '?fn=getUserPrivilegies';
            this.http.post(url, postData,(response) => {
                this.userPrivilegies = response;
            });
        },

        htmlElemsRender(selector, func) {
            let elements = document.querySelectorAll(selector)
            elements.forEach(func);
        },

    }
}
