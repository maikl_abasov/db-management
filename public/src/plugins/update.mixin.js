
const UpdateMixin = {
    data() {
        return {
            // http: httpClient,
            newDbName: '',
            copyTableName: '',

            newTable: {
                table_name: '',
                id_name: 'id',
            },

            newUser: {
                username : '',
                password: '123456',
                set_super_user: 1,
            },

            newField : {
                'table_name': '',
                'field_name': '',
                'type': 'varchar',
                'size': 255,
                'is_null': 1,
                'default_value': '',
            },
        }
    },

    created() {
    },

    methods: {

        sendChange(postData, funcName, callback) {
            const url = '?fn=' + funcName;
            this.http.post(url, postData,(response) => {
                if(response.queryString) {
                    callback(response);
                }
                console.log(response)
            });
        },

        dropField(item) {
            const postData = { table_name: this.tableName, field_name: item.field_name };
            const url = '?fn=dropField';
            this.http.post(url, postData,(response) => {
                 if(response.queryString) {
                     this.getFields();
                     // alert(response.queryString);
                 }
                 console.log(response)
            });
        },

        dropTable() {
            const postData = { table_name: this.tableName };
            const url = '?fn=dropTable';
            this.http.post(url, postData,(response) => {
                if(response.queryString) {
                    this.getTables();
                    this.tablePageClear();
                    alert(response.queryString);
                }
                console.log(response)
            });
        },

        createTable() {
            const postData = this.newTable;
            const url = '?fn=createTable';
            this.http.post(url, postData,(response) => {
                if(response.queryString) {
                    this.getTables();
                    alert(response.queryString);
                }
                console.log(response)
            });
        },

        createUser() {
            const postData = this.newUser;
            const url = '?fn=createUser';
            this.http.post(url, postData,(response) => {
                if(response.queryString) {
                    this.getDbUsersList();
                    alert(response.queryString);
                }
                console.log(response)
            });
        },

        createDatabase() {
            const postData = { db_name: this.newDbName };
            const url = '?fn=createDatabase';
            this.http.post(url, postData,(response) => {
                if(response.queryString) {
                    this.getDatabaseList();
                    alert(response.queryString);
                }
                console.log(response)
            });
        },

        addField() {
            const postData = this.newField;
            this.newField.table_name = this.tableName;
            const url = '?fn=addField';
            this.http.post(url, postData,(response) => {
                if(response.queryString) {
                    this.getFields();
                    alert(response.queryString);
                }
                console.log(response)
            });
        },

        copyTable() {
            const postData = { table_name: this.tableName, copy_table_name: this.copyTableName };
            const url = '?fn=copyTable';
            this.http.post(url, postData,(response) => {
                if(response.queryString) {
                    this.getTables();
                    alert(response.queryString);
                }
                console.log(response)
            });
        },

        deleteDbUser() {
            let userName = this.userName
            const postData = { username: userName};
            const url = '?fn=deleteDbUser';
            this.http.post(url, postData,(response) => {
                if(response.queryString) {
                    this.getDbUsersList();
                    this.userPageClear();
                    alert(response.queryString);
                }
                console.log(response)
            });
        },

        dropDb() {
            let dbName = this.getDbName
            const postData = { db_name: dbName};
            const url = '?fn=dropDb';
            this.http.post(url, postData,(response) => {
                if(response.queryString) {
                    this.getDatabaseList();
                    this.dbName = '';
                    this.dbItem = {};
                    this.databaseList = [];
                    alert(response.queryString);
                }
                console.log(response)
            });
        },

        userPageClear() {
            this.userName = ''
            this.userItem = {};
        },

        tablePageClear() {
            this.tableName = '';
            this.fieldName = '';
            this.tableItem = [];
            this.fieldsList = [];
        },

    }
}
