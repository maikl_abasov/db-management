
class Http {

    constructor(apiUrl) {
        this.apiUrl = apiUrl;
    }

    get(url, callback = null) {
        const apiUrl = this.apiUrl + url;
        axios.get(apiUrl).then((response) => {
            let result = this.responseHandle(response);
            if(result) callback(result);
        }).catch((error) => {
            lg(error);
            console.log(error);
        })
    }

    post(url, postData, callback = null) {
        const apiUrl = this.apiUrl + url;
        axios.post(apiUrl, postData).then((response) => {
            let result = this.responseHandle(response);
            if(result) callback(result);
        }).catch((error) => {
            lg(error);
            console.log(error);
        });
    }

    responseHandle(response) {
        // console.log(response);
        let data = response.data;
        if(data.error) {
            lg(data.error);
            return false
        }
        return data.result;
    }

}