
const API_URL = 'http://my-framework/db-management/db-management/api.php';
const httpClient = new Http(API_URL);

const {createApp} = Vue;

const App = createApp({
    // store: GlobalStore,
    data() {
        return {
            urlHash: '',
            url : '#table-page',
            currentComponent: '',
        }
    },

    created() {
    },

    mounted() {
        this.urlHash = window.location.hash;
        this.loadPage(this.urlHash);
    },

    computed: {
        getUrlHash() {
            return this.urlHash;
        }
    },

    methods: {

        sendResponse(response) {
            console.log(response);
        },

        loadPage(url = '#table-page', event = null) {

            let activeClass = 'top-menu-active';

            if(event) {
                this.htmlElemsRender('.top-menu-list', (elem) => {
                    elem.classList.remove(activeClass);
                });
                event.target.classList.add(activeClass);
            } else {
                this.htmlElemsRender('.top-menu-list', (elem) => {
                    if(elem.hash == url) {
                        elem.classList.add(activeClass);
                    }
                });
            }

            this.urlHash = url;

            switch (url) {
                case '#table-page' :
                    this.currentComponent = TablePage;
                    break;

                case '#user-page' :
                    this.currentComponent = UserPage;
                    break;

                case '#database-page' :
                    this.currentComponent = DataBasePage;
                    break;

                case '#migrate-page' :
                    this.currentComponent = MigratePage;
                    break;

                case '#settings-page' :
                    this.currentComponent = SettingsPage;
                    break;

                default :
                    this.currentComponent = TablePage;
                    break;
            }
        },
    },

    components: {
        TablePage,
        UserPage,
        DataBasePage,
        MigratePage,
        SettingsPage
    },

});

App.mixin(GlobalMixin);
App.mixin(UpdateMixin);
App.component('list-render', ListRender);
App.component('edit-form-wave', EditFormWave);
App.mount('#vue-app');
