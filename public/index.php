<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Message queue service</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://unpkg.com/vue@3"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<!--    <script src="https://cdn.bootcdn.net/ajax/libs/vuex/4.0.2/vuex.global.js">-->

    <script></script>
    <style>

        .top-menu-active {
            border-bottom: 2px white solid;
        }

        .tab-field-box {
            border-bottom: 1px gainsboro solid;
            width: 80px;
        }

        .edit-form-item-wave-box {
            border: 1px gainsboro solid;
            padding: 8px 8px 0px 8px;
            border-radius: 3px;
        }

        .btn-small-size {
            border-radius: 3px;
            padding: 0px 8px 0px 8px !important;
            font-size: 11px !important;
            height: 40px;
            text-align: center !important;
        }

    </style>

</head>
<body>
<div id="vue-app" class="wrapper">

    <div class="bg-white">

        <header class="relative bg-white" style="margin-bottom: 20px">
            <div class="bg-gray-800 h-15 text-sm font-medium text-white" style="">
              <nav class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 text-white" >
                <div class="border-b border-gray-800">
                    <div class="h-16 flex items-center">
                        <button type="button" class="bg-white p-2 rounded-md text-gray-400 lg:hidden">
                            <span class="sr-only">Open menu</span>
                            <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M4 6h16M4 12h16M4 18h16" />
                            </svg>
                        </button>
                        <!-- Logo -->
                        <div class="ml-4 flex lg:ml-0">
                            <a href="#">
                                <span class="sr-only">Workflow</span>
                                <img class="h-8 w-auto" src="https://tailwindui.com/img/logos/workflow-mark.svg?color=indigo&shade=600" alt="">
                            </a>
                        </div>
                        <!-- Flyout menus -->
                        <div class="hidden lg:ml-8 lg:block lg:self-stretch">
                            <div class="h-full flex space-x-8">
                                <a href="#table-page" @click="loadPage('#table-page', $event)" class="flex items-center text-sm font-medium hover:text-gray-400 top-menu-list" style="">Таблицы</a>
                                <a href="#user-page" @click="loadPage('#user-page', $event)" class="flex items-center text-sm font-medium hover:text-gray-400 top-menu-list" >Пользователи</a>
                                <a href="#database-page" @click="loadPage('#database-page', $event)"class="flex items-center text-sm font-medium hover:text-gray-400 top-menu-list">Базы</a>
                                <a href="#migrate-page" @click="loadPage('#migrate-page', $event)" class="flex items-center text-sm font-medium hover:text-gray-400 top-menu-list">Миграции</a>
                                <a href="#settings-page" @click="loadPage('#settings-page', $event)" class="flex items-center text-sm font-medium hover:text-gray-400 top-menu-list">Настройка</a>
                            </div>
                        </div>
                        <div class="ml-auto flex items-center">

                            <div class="hidden lg:flex lg:flex-1 lg:items-center lg:justify-end lg:space-x-6">
                                <a href="#" class="text-sm font-medium hover:text-gray-400">Войти</a>
                                <span class="h-6 w-px bg-gray-200" aria-hidden="true"></span>
                                <a href="#" class="text-sm font-medium hover:text-gray-400">Регистрация</a>
                            </div>

                            <!-- Search -->
                            <div class="flex lg:ml-6">
                                <a href="#" class="p-2 text-gray-400 hover:text-gray-500">
                                    <span class="sr-only">Search</span>
                                    <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" aria-hidden="true">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                                    </svg>
                                </a>
                            </div>

                            <!-- Cart -->
                            <div class="ml-4 flow-root lg:ml-6">
                                <a href="#" class="group -m-2 p-2 flex items-center">

                                    <svg class="flex-shrink-0 h-6 w-6 text-gray-400 group-hover:text-gray-500" xmlns="http://www.w3.org/2000/svg"  fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M8 10h.01M12 10h.01M16 10h.01M9 16H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-5l-5 5v-5z" />
                                    </svg>

                                    <span class="ml-2 text-sm font-medium text-gray-700 group-hover:text-gray-800">0</span>
                                    <span class="sr-only">items in cart, view bag</span>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
              </nav>
          </div>
        </header>

    </div><!--- bg-white --->

    <div class="bg-white">

        <main class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <section aria-labelledby="products-heading" class="pt-6 pb-24">
                <div style="display: flex; margin-bottom: 20px; border-bottom: 2px gainsboro solid; padding: 0px 0px 10px 0px">

                    <template v-if="getUrlHash == '#user-page'">
                        <edit-form-wave
                                style="width: 40%; border: 0px"
                                btn_title="Создать пользователя"
                                :form_item="{ username: '', password: '123456', set_super_user: '1' }"
                                func_name="createUser"
                                cols="4" gap="2"
                                @send_response="sendResponse" />
                    </template>
                    <template v-else-if="getUrlHash == '#table-page'">
                        <edit-form-wave
                                style="width: 40%; border: 0px"
                                btn_title="Создать таблицу"
                                :form_item="{ table_name: '', id_name: 'id'}"
                                func_name="createTable"
                                cols="4" gap="2"
                                @send_response="sendResponse" />
                    </template>
                    <template v-else-if="getUrlHash == '#database-page'">
                        <edit-form-wave
                                style="width: 40%; border: 0px"
                                btn_title="Создать базу"
                                :form_item="{ db_name: ''}"
                                func_name="createDatabase"
                                cols="4" gap="2"
                                @send_response="sendResponse" />
                    </template>



                    <!-----
                    <div style="display: flex; width: 35%; border: 0px gainsboro solid; padding: 3px;">
                        <div class="relative" style=" width: 120px">
                            <input v-model="newTable.table_name" type="text" id="create_table_name" :class="inputClassWave" placeholder=" " />
                            <label for="create_table_name" :class="labelClassWave">Имя таблицы</label>
                        </div>
                        <div class="relative" style="margin-left: 5px; width: 70px">
                            <input v-model="newTable.id_name" type="text" id="create_table_id" :class="inputClassWave" placeholder=" " />
                            <label for="create_table_id" :class="labelClassWave">Id name</label>
                        </div>
                        <a href="#" @click="createTable()" :class="btnClassWave"
                           style="border-radius: 3px; padding: 0px 8px 0px 8px; font-size: 11px; height: 40px">Создать таблицу</a>
                    </div>

                    <div style="display: flex; width: 40%; border: 0px gainsboro solid; padding: 3px;">
                        <div class="relative" style=" width: 150px">
                            <input v-model="newUser.username" type="text" id="create_user_name" :class="inputClassWave" placeholder=" " />
                            <label for="create_user_name" :class="labelClassWave">Имя пользователя</label>
                        </div>

                        <div class="relative" style="margin-left: 5px; width: 100px">
                            <input v-model="newUser.password" type="text" id="create_user_password" :class="inputClassWave" placeholder=" " />
                            <label for="create_user_password" :class="labelClassWave">Пароль</label>
                        </div>

                        <div class="relative" style="margin-left: 5px; width: 70px">
                            <input v-model="newUser.set_super_user" type="text" id="create_user_root" :class="inputClassWave" placeholder=" " />
                            <label for="create_user_root" :class="labelClassWave">Права</label>
                        </div>

                        <a href="#" @click="createUser()" :class="btnClassWave"
                           style="border-radius: 3px; padding: 0px 8px 0px 8px; font-size: 11px; height: 40px">Создать пользователя</a>
                    </div>

                    <div style="display: flex; width: 35%; border: 0px gainsboro solid; padding: 3px; margin-left:20px;">
                        <div class="relative" style=" width: 120px">
                            <input v-model="newDbName" type="text" id="create_db_name" :class="inputClassWave" placeholder=" " />
                            <label for="create_db_name" :class="labelClassWave">Имя базы</label>
                        </div>
                        <a href="#" @click="createDatabase()" :class="btnClassWave"
                           style="border-radius: 3px; padding: 0px 8px 0px 8px; font-size: 11px; height: 40px">Создать базу</a>
                    </div>

                    --->

<!--                    <div class="relative">-->
<!--                        <input type="text" id="floating_outlined" class="block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " />-->
<!--                        <label for="floating_outlined" class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1">Floating outlined</label>-->
<!--                    </div>-->
<!--                    <div class="relative z-0">-->
<!--                        <input type="text" id="floating_standard" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " />-->
<!--                        <label for="floating_standard" class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Floating standard</label>-->
<!--                    </div>-->

                </div>

                <div class="grid grid-cols-1 lg:grid-cols-4 gap-x-8 gap-y-10">
                    <component :is="currentComponent"></component>
                </div>

            </section>
        </main>
    </div>

</div>
</body>

<script src="src/http.js" type="application/javascript"></script>
<!--<script src="src/store.js" type="application/javascript"></script>-->

<script src="src/plugins/lg.js" type="application/javascript"></script>
<script src="src/plugins/update.mixin.js" type="application/javascript"></script>
<script src="src/plugins/global.mixin.js" type="application/javascript"></script>

<script src="src/components/global.components.js" type="application/javascript"></script>
<script src="src/components/list.render.js" type="application/javascript"></script>
<script src="src/components/edit.form.wave.js" type="application/javascript"></script>

<script src="src/pages/settings.js" type="application/javascript"></script>
<script src="src/pages/databases.js" type="application/javascript"></script>
<script src="src/pages/migrate.js" type="application/javascript"></script>
<script src="src/pages/tables.js" type="application/javascript"></script>
<script src="src/pages/users.js" type="application/javascript"></script>

<script src="src/app.js" type="application/javascript"></script>

</html>





