<?php

namespace App\Service\Db\Management;

use App\Service\Db\Management\Traits\DbUsers;
use App\Service\Db\Management\Traits\DbTableHandlers;
use App\Service\Db\Management\Traits\DbHandlers;
use App\Service\Db\Management\Traits\DbDataContentHandlers;
use App\Service\Db\Management\Traits\DbMigrate;

class DbManagementService
{

    use DbUsers;
    use DbHandlers;
    use DbTableHandlers;
    use DbDataContentHandlers;
    use DbMigrate;

    protected $db;
    protected $post   = [];
    protected $config = [];
    protected $driver = 'mysql';
    protected $dbName;
    protected $memory;

    public function __construct($config, $post = array()) {
        $this->config = $config;
        $this->driver = $this->isValue($this->config, 'driver');
        $this->dbName = $this->isValue($this->config, 'db_name');
        $this->connection();
        if(!empty($post)) $this->post = $post;
        else $this->getInputData();
        $this->memory = new MemoryMonitorService();
    }

    public function dispatch($funcName = null, $params = []) {

        if(!$funcName) {
            throw new CustomErrorHandler('Dispatch: не задано имя функции', 5530, __FILE__, __LINE__, []);
        }

        if(!empty($params)) $data = $this->$funcName($params);
        else   $data = $this->$funcName();
        return $data;
        // return $this->getResponse($data);
    }

    // Получить список таблиц
    protected function getTables($params = array()) {
        $scheme = $this->isParam($params, 'scheme', 'public');
        $dbName = $this->dbName;
        $where = ($this->driver == 'mysql') ?
            "WHERE table_schema ='{$dbName}'" :
            "WHERE table_schema='{$scheme}'";
        $sql = "SELECT * FROM INFORMATION_SCHEMA.TABLES " . $where;

        $tables =  $this->select($sql);

        $result = [];
        foreach ($tables as  $elem) {
            $item    = $this->toLowerName($elem);
            $tabName = $item['table_name'];
            $result[$tabName] = $item;
        }
        return $result;
    }

    // Получить поля таблицы
    protected function getFields($params = array()) {
        $tableName = $this->isParam($params, 'table_name');
        if(!$tableName) {
            if(!$tableName) return false;
        }

        $sql = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='{$tableName}'";
        $fields = $this->select($sql);

        $result = [];
        foreach ($fields as  $elem) {
            $item = $this->toLowerName($elem);
            $auto = 0;
            if(!empty($item['extra']) && $item['extra'] == 'auto_increment') $auto = 1;
            elseif(!empty($item['column_default'])) $auto = 1;

            $columnName = $item['column_name'];
            $columnType = $item['data_type'];

            $item['auto_increment'] = $auto;
            $item['field_name']     = $columnName;
            $item['field_type']     = $columnType;

            $result[$columnName] = $item;
        }
        return $result;
    }

    // Получить список таблиц с полями
    protected function getTablesInfo($params = array()) {
        $scheme = $this->isParam($params, 'scheme', 'public');
        $where = ($this->driver == 'mysql') ?
            "WHERE TABLE_SCHEMA != 'information_schema'" :
            "WHERE table_schema='{$scheme}'";

        $sql = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS " . $where;
        $tablesInfo =  $this->select($sql);

        $result = $props = [];

        foreach ($tablesInfo as  $elem) {
            $item    = $this->toLowerName($elem);

            $auto = 0;
            if(!empty($item['extra']) && $item['extra'] == 'auto_increment') $auto = 1;
            elseif(!empty($item['column_default'])) $auto = 1;

            $tabName    = $item['table_name'];
            $columnName = $item['column_name'];
            $columnType = $item['data_type'];

            $item['auto_increment'] = $auto;
            $item['field_name']     = $columnName;
            $item['field_type']     = $columnType;
            $props[$columnType] = $columnType;

            $result[$tabName][$columnName] = $item;
            // $result[$tabName] = $tabName;
        }

        return $result;
    }

    /****
     * @param $sql SELECT * FROM `category` WHERE `id` = :id
     * @param array $params  array('id' => '21')
     */
    protected function make($sql, $params = []) {
        try {
            $sth = $this->db->prepare($sql);
            $sth->execute($params);
        } catch (\Exception $err) {
            $error = $this->errorFormat($err, $sql, $params,  'DB ERROR');
            $error = json_encode($error);
            throw new \ErrorException($error, 101);
        }
        return $sth;
    }

    protected function select($sql, $params = []) {
        $sth = $this->make($sql, $params);
        $result = $sth->fetchAll();
        return $result;
    }

    protected function selectOne($sql, $params = []) {
        $sth = $this->make($sql, $params);
        $result = $sth->fetch();
        return $result;
    }

    public function connection($config = []) {
        $config = (!empty($config)) ? $config : $this->config;
        $this->driver = $config['driver'];
        try {
            $dsn = $config['driver'] . ':dbname=' .$config['db_name']. ';host=' . $config['db_host'];
            $this->db = new \PDO($dsn, $config['db_user'], $config['db_password']);
            $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        } catch (PDOException $err) {
            $errMessage = $err->getMessage();
            $this->getError($errMessage);
        }
    }

    protected function getInputData() {
        $rawData = file_get_contents("php://input");
        $this->post =  json_decode($rawData);
    }

    public function getResponse($data) {
        $this->response($data, 'result');
    }

    public function getError($data) {
        $this->response($data, 'error');
    }

    protected function response($data, $fieldName = null) {
        if($fieldName) $result[$fieldName] = $data;
        else   $result = $data;
        die(json_encode($result,  JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
    }

    protected function isValue($data, $field, $defaultValue = false) {
       $value =  (!empty($data[$field])) ? $data[$field] : '';
       if(!$value && $defaultValue)
           $value = $defaultValue;
       return $value;
    }

    protected function isParam($data, $field, $defaultValue = false) {
        $value =  (!empty($data[$field])) ? $data[$field] : '';
        if(!$value && $defaultValue)
            $value = $defaultValue;
        return $value;
    }

    protected function isPostValue($field) {
        $value =  (!empty($this->post[$field])) ? $this->post[$field] : '';
        return $value;
    }

    protected function isDriver($data) {
        $value = '';
        switch ($this->driver) {
            case 'mysql' : $value = $this->isValue($data, 'mysql'); break;
            case 'pgsql' : $value = $this->isValue($data, 'pgsql'); break;
        }
        return $value;
    }

    protected function fileSave($fileName, $data) {
        return file_put_contents($fileName, $data);
    }

    protected function toLowerName($item) {
        $result = [];
        foreach ($item as $fname => $value) {
            $fname = strtolower($fname);
            $result[$fname] = $value;
        }
        return $result;
    }

    protected function errorFormat($err, $sql = '', $params = [], $type = 'DB ERROR') {
        $error = [
            'err_type' => $type,
            'sql'      => $sql,
            'message'  => $err->getMessage(),
            'code'     => $err->getCode(),
            'file'     => $err->getFile(),
            'line'     => $err->getLine(),
            'params'   => $params,
            'previos'  => $err->getPrevious(),
            'trace'    => $err->getTrace(),
        ];
        return $error;
    }

    protected function getTexAreaContentSeparate($content) {
        $array = explode("\n", $content);
        return $array;
    }

    protected function freeSqlMake($params) {
        $query = $this->isParam($params, 'query');
        $type  = $this->isParam($params, 'type');
        switch ($type) {
            case 'select' :
                $result = $this->select($query);
                break;

            case 'make' :
            case 'exec' :
                $result = $this->make($query);
                break;
        }
        return $result;
    }

}


class MemoryMonitorService {

   public $message;
   public $info = [];
   public $start = 0;

   public function run($title = '') {

       $time = microtime(true);
       $memory = memory_get_usage();
       $memoryCompare = '';
       if($this->start) {
           $lastElem = end($this->info);
           $lastMemory = $lastElem['memory'];
           $lastMemory = $memory - $lastMemory;
           $memoryCompare = $this->memoryConvert($lastMemory);
       }

       $memoryConvert = $this->memoryConvert($memory);
       $item = [
           'title'  => $title,
           'time'   => $time,
           'memory' => $memory,
           'date'   => date("Y-m-d H:i:s"),
           'memory_convert' => $memoryConvert,
           'memory_compare' => $memoryCompare,
       ];

       $this->info[] = $item;
       $this->start  = 1;
   }

    public function memoryConvert($memory) {
        $name = ['байт', 'кб', 'мб'];
        $i = 0;
        while (floor($memory / 1024) > 0) {
            $i++;
            $memory /= 1024;
        }
        $message = 'Использовано памяти: ' . round($memory, 2) . ' ' . $name[$i];
        return $message;
    }

    public function show() {

        $first = $this->info[0];
        $last  = end($this->info);

        $fisrtMemory = $first['memory'];
        $lastMemory  = $last['memory'];

        $fisrtTime = $first['time'];
        $lastTime  = $last['time'];

        $resTime = $lastTime - $fisrtTime;

        lg([
            'FINAL' => ['first' => $fisrtMemory, 'last' => $lastMemory, 'res_time' =>  $resTime],
            'INFO'  => $this->info,
        ]);
    }

}


/***********
 *  postgres = Array (
        [table_catalog] => user_profile_db
        [table_schema] => public
        [table_name] => migrations
        [column_name] => id
        [ordinal_position] => 1
        [column_default] => nextval('migrations_id_seq'::regclass)
        [is_nullable] => NO
        [data_type] => integer
        [character_maximum_length] =>
        [character_octet_length] =>
        [numeric_precision] => 32
        [numeric_precision_radix] => 2
        [numeric_scale] => 0
        [datetime_precision] =>
        [interval_type] =>
        [interval_precision] =>
        [character_set_catalog] =>
        [character_set_schema] =>
        [character_set_name] =>
        [collation_catalog] =>
        [collation_schema] =>
        [collation_name] =>
        [domain_catalog] =>
        [domain_schema] =>
        [domain_name] =>
        [udt_catalog] => user_profile_db
        [udt_schema] => pg_catalog
        [udt_name] => int4
        [scope_catalog] =>
        [scope_schema] =>
        [scope_name] =>
        [maximum_cardinality] =>
        [dtd_identifier] => 1
        [is_self_referencing] => NO
        [is_identity] => NO
        [identity_generation] =>
        [identity_start] =>
        [identity_increment] =>
        [identity_maximum] =>
        [identity_minimum] =>
        [identity_cycle] =>
        [is_generated] => NEVER
        [generation_expression] =>
        [is_updatable] => YES

        [autoincrement] => 1
        [field_name] => id
        [field_type] => integer
  )
 */


/***********
 * mysql = Array (
        [TABLE_CATALOG] => def
        [TABLE_SCHEMA] => bolderp5_rrt_v2
        [TABLE_NAME] => autoru_flat_modifications
        [COLUMN_NAME] => id
        [ORDINAL_POSITION] => 1
        [COLUMN_DEFAULT] =>
        [IS_NULLABLE] => NO
        [DATA_TYPE] => int
        [CHARACTER_MAXIMUM_LENGTH] =>
        [CHARACTER_OCTET_LENGTH] =>
        [NUMERIC_PRECISION] => 10
        [NUMERIC_SCALE] => 0
        [DATETIME_PRECISION] =>
        [CHARACTER_SET_NAME] =>
        [COLLATION_NAME] =>
        [COLUMN_TYPE] => int(11)
        [COLUMN_KEY] => PRI
        [EXTRA] => auto_increment
        [PRIVILEGES] => select,insert,update,references
        [COLUMN_COMMENT] =>
        [GENERATION_EXPRESSION] =>

        [autoincrement] => 1
        [field_name] => id
        [field_type] => int
   )
 */