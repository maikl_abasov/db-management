<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
// header('Content-Type: text/html; charset=utf-8');
header('Content-Type: application/json');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

set_time_limit(0);

define('ROOT_DIR', __DIR__);
define('MIGRATE_DIR', ROOT_DIR . '/migrate');
define('LOGS_DIR', ROOT_DIR . '/logs');
define('LOG_FILE_NAME', 'logs.txt');

require_once ROOT_DIR . '/helpers.php';
require_once ROOT_DIR . '/traits/DbHandlers.php';
require_once ROOT_DIR . '/traits/DbTableHandlers.php';
require_once ROOT_DIR . '/traits/DbUsers.php';
require_once ROOT_DIR . '/traits/DbDataContentHandlers.php';
require_once ROOT_DIR . '/traits/DbMigrate.php';
require_once ROOT_DIR . '/DbManagementService.php';
require_once ROOT_DIR . '/CustomErrorHandler.php';

$dbConfig = require_once ROOT_DIR . '/config.php';

use App\Service\Db\Management\CustomErrorHandler;
use App\Service\Db\Management\DbManagementService;

$dbService = new DbManagementService($dbConfig);

$funcName = '';
if(!empty($_GET['fn'])) {
    $funcName = $_GET['fn'];
}

$params = getInputData();

////////////////////////
///
///
//$tableName    = 'fist_table';
//$newTableName = 'second_table';
//
//$fieldName    = 'first_name11';
//$newFieldName = 'second_name';

// $funcName = 'import';

//$funcName = 'export';
//$params   = ['db_name' => 'user_profile_db'];
//
//$funcName = 'createTable';
//$params   = ['table_name' => $tableName];

//$funcName = 'renameTable';
//$params   = ['table_name' => $tableName, 'new_table_name' => $newTableName];
//
//$funcName = 'dropTable';
//$params   = ['table_name' => $newTableName];

//$funcName = 'addField';
//$params   = ['table_name' => $tableName, 'field_name' => $fieldName, 'type' => 'VARCHAR', 'size' => 350];

//$funcName = 'renameField';
//$params   = ['table_name' => $tableName, 'old_name' => $fieldName, 'new_name' => $newFieldName];

//$funcName = 'dropField';
//$params   = ['table_name' => $tableName, 'field_name' => $newFieldName];

//$funcName = 'changeFieldType';
//$params   = ['table_name' => $tableName, 'field_name' => $newFieldName, 'new_type' => 'varchar'];

//$funcName = 'createIndex';
//$params   = ['table_name' => $tableName, 'field_name' => $fieldName];

//$funcName = 'dropIndex';
//$params   = ['table_name' => $tableName, 'field_name' => 'first_name'];

//$funcName = 'dropIndex';
//$params   = ['table_name' => $tableName, 'field_name' => 'first_name'];

//$funcName = 'getDatabaseList';
//$params   = [];

//$funcName = 'getСurrentDatabase';
//$params   = [];

//$funcName = 'createDatabase';
//$params   = ['db_name' => 'db2'];

//$funcName = 'createDbNextBindUser';
//$params   = ['db_name' => 'db33', 'username' => 'root'];

//$funcName = 'dropDb';
//$params   = ['db_name' => 'db2'];

//$funcName = 'createDbNextCreateUser';
//$params   = ['db_name' => 'db33', 'username' => 'db33_maikl', 'password' => '123456'];

//$funcName = 'copyDb';
//$params   = ['db_name' => 'openserver_pgsql', 'username' => 'maikladmin', 'new_db_name' => 'os_pgsql2'];

//$funcName = 'getDbUsersList';
//$params   = [];

//$funcName = 'getCurrentDbUser';
//$params   = [];

//$funcName = 'createUser';
//$params   = ['username' => 'sem66', 'password' => '123456', 'set_super_user' => true];

//$funcName = 'getUserPrivilegies';
//$params   = ['username' => 'maikladmin'];

//$funcName = 'freeSqlMake';
//$params   = ['query' => 'SELECT * FROM users', 'type' => 'select'];

set_error_handler('myCustomErrorHandler', E_ALL);
//set_error_handler("customErrorWarningHandler"); // обработка предупреждений
//register_shutdown_function('customFatalErrorHanler'); // обработка всех ошибок в том числе и фатальных

try {
   $response = $dbService->dispatch($funcName, $params);
} catch (\Exception $err) {

    if(empty($err->customError)) {
        $json = json_decode($err->getMessage());
        $error = [
            'code'    => $err->getCode(),
            'message' => $json->message,
            'file'    => $err->getFile(),
            'line'    => $err->getLine(),
            'error'   => $json,
        ];
    } else {
        $error = [
            'code'    => $err->getCode(),
            'message' => $err->getMessage(),
            'file'    => $err->getFile(),
            'line'    => $err->getLine(),
            'error'   => $err->customError,
        ];
    }
    $dbService->getError($error);

}

$dbService->getResponse($response);

/////////////////////
////////////////////





