<?php

namespace App\Service\Db\Management;

class CustomErrorHandler extends \ErrorException
{

    public $myFile;
    public $myLine;
    public $myMessage;
    public $myCode;
    public $customError;

    public function __construct($message, $errorLevel = 0, $errorFile = '', $errorLine = 0, $params = []) {
        parent::__construct($message, $errorLevel);
        $this->myFile    = $errorFile;
        $this->myLine    = $errorLine;
        $this->myMessage = $message;
        $this->myCode    = $errorLevel;
        $this->errorFormat($params);
    }

    protected function errorFormat($params) {
        $type  = (!empty($params['type']))  ? $params['type']  : '';
        $sql   = (!empty($params['sql']))   ? $params['sql']   : '';
        $title = (!empty($params['title'])) ? $params['title'] : '';
        $err = $this;
        $this->customError = [
            'my_type'  => $type,
            'my_title' => $title,
            'sql'      => $sql,
            'message'  => $err->getMessage(),
            'code'     => $err->getCode(),
            'file'     => $err->getFile(),
            'line'     => $err->getLine(),
            'params'   => $params,
            'previos'  => $err->getPrevious(),
            'trace'    => $err->getTrace(),
        ];
        return $this->customError;
    }

}